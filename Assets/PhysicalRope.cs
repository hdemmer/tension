﻿using UnityEngine;
using System.Collections;

public class PhysicalRope : MonoBehaviour {
	public Rigidbody2D origin;
	public Rigidbody2D target;
	public float length;
	public int numPoints;

	LineRenderer lineRenderer;
	Transform[] ropeTransforms;

	void Start()
	{
		if (numPoints < 2)
		{
			numPoints = 2;
		}

		ropeTransforms = new Transform[numPoints];
		ropeTransforms[0] = origin.transform;
		
		var segmentLength = (length / (numPoints-1));

		var previousGO = origin.gameObject;
		for (int i=1; i < numPoints;i++)
		{
			var go = target.gameObject;

			if (i != numPoints - 1)
			{
				go = new GameObject("Rope " + i);
				go.transform.SetParent(transform);
				go.transform.position = Vector3.Lerp(origin.transform.position, target.transform.position, i/(float)(numPoints-1));
			}

			ropeTransforms[i] = go.transform;

			var distanceJoint = go.gameObject.AddComponent<DistanceJoint2D>();
			distanceJoint.connectedBody = previousGO.GetComponent<Rigidbody2D>();
			distanceJoint.maxDistanceOnly = true;
			distanceJoint.distance = segmentLength;
			previousGO = go;

		}

		lineRenderer = gameObject.AddComponent<LineRenderer>();
		lineRenderer.SetWidth(0.1f,0.1f);
		lineRenderer.SetVertexCount(numPoints);
	}

	void Update()
	{
		for (int i=0; i < numPoints;i++)
		{
			lineRenderer.SetPosition(i,ropeTransforms[i].position);
		}
	}
}
