﻿using UnityEngine;
using System.Collections;

public class VerletRope : MonoBehaviour
{
	public Rigidbody2D origin;
	public Vector2 originOffset;
	public Rigidbody2D target;
	public Vector2 targetOffset;
	public float length;
	public int numPoints;

	public Vector2[] positions;
	Vector2[] previous;

	float segmentLength;

	void Start()
	{
		if (numPoints < 2)
		{
			numPoints = 2;
		}

		positions = new Vector2[numPoints];
		previous = new Vector2[numPoints];

		segmentLength = (length / (numPoints-1));

		// calculate rotated offsets (copy/pasted from FixedUpdate)
		var originAngle = origin.transform.localRotation.eulerAngles.z / 180f * Mathf.PI;
		var targetAngle = target.transform.localRotation.eulerAngles.z / 180f * Mathf.PI;

		var cao = Mathf.Cos( originAngle );
		var sao = Mathf.Sin( originAngle );
		var rotatedOriginOffset = new Vector2(cao * originOffset.x - 1 * sao * originOffset.y, sao * originOffset.x + cao * originOffset.y);

		var cat = Mathf.Cos( targetAngle );
		var sat = Mathf.Sin( targetAngle );
		var rotatedTargetOffset = new Vector2(cat * targetOffset.x - sat * targetOffset.y, sat * targetOffset.x + cat * targetOffset.y);

		var originV2 = new Vector2(origin.transform.position.x, origin.transform.position.y) + rotatedOriginOffset;
		var targetV2 = new Vector2(target.transform.position.x, target.transform.position.y) + rotatedTargetOffset;

		for (int i=0; i<numPoints; i++)
		{
			positions[i] = Vector2.Lerp(originV2, targetV2, i/(float)(numPoints-1));
			previous[i] = positions[i];
		}
	}

	void Verlet()
	{
		var dt = Time.fixedDeltaTime;

		var grav = Vector2.down * 9.81f * dt * dt;
		for (int i=1; i<numPoints; i++)
		{
			var storedCurrent = positions[i];

			positions[i] = 2 * storedCurrent - previous[i] + grav;
			previous[i] = storedCurrent;
		}
	}

	void Constraints()
	{
		for (int i=0; i<numPoints-1; i++)
		{
			var d = (positions[i+1] - positions[i]);
			var dMag = d.magnitude;
			var d3 = (dMag - segmentLength)/dMag;
			if (d3 > 0f)	// max distance only
			{
				positions[i] = positions[i] + (0.5f * d3) * d;
				positions[i+1] = positions[i+1] - (0.5f * d3) * d;
			}
		}
	}

	Vector2 VelocityAt(int index)
	{
		return 0.5f * (positions[index] - previous[index]) / Time.fixedDeltaTime;
	}

	void FixedUpdate()
	{
		var originAngle = origin.transform.localRotation.eulerAngles.z / 180f * Mathf.PI;
		var targetAngle = target.transform.localRotation.eulerAngles.z / 180f * Mathf.PI;

		var cao = Mathf.Cos( originAngle );
		var sao = Mathf.Sin( originAngle );
		var rotatedOriginOffset = new Vector2(cao * originOffset.x - 1 * sao * originOffset.y, sao * originOffset.x + cao * originOffset.y);

		var cat = Mathf.Cos( targetAngle );
		var sat = Mathf.Sin( targetAngle );
		var rotatedTargetOffset = new Vector2(cat * targetOffset.x - sat * targetOffset.y, sat * targetOffset.x + cat * targetOffset.y);

		var originV2 = new Vector2(origin.transform.position.x, origin.transform.position.y) + rotatedOriginOffset;
		var targetV2 = new Vector2(target.transform.position.x, target.transform.position.y) + rotatedTargetOffset;

		positions[0] = originV2;
		positions[numPoints-1]=targetV2;
		Verlet();
		for (int i=0; i<3;i++)
		{
			Constraints();
			positions[0] = originV2;
			positions[numPoints-1] = targetV2;
		}
	}
}
