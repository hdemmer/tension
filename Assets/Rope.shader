﻿Shader "Custom/Rope" {
	Properties {
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader {
        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            
            uniform sampler2D _MainTex;
            
        	v2f_img vert( appdata_img v ) {
        		v2f_img o;
        		o.pos = mul (UNITY_MATRIX_VP, v.vertex); 
        		o.uv = MultiplyUV( UNITY_MATRIX_TEXTURE0, v.texcoord );
        		return o; 
    		} 

            fixed4 frag(v2f_img i) : SV_Target {
                return tex2D(_MainTex, i.uv);
            }
            ENDCG
        }
    }
}
