﻿using UnityEngine;
using System.Collections;

public class VerletRopeRenderer : MonoBehaviour {

	VerletRope rope;
	MeshFilter meshFilter;

	public float thickness = 0.1f;

	int vertexCount;
	Vector3[] vertices;

	void Start () {
		transform.parent = null;
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		transform.localScale = Vector3.one;

		rope = GetComponent<VerletRope>();

		var numPoints = rope.numPoints;
		var length = rope.length;

		Mesh mesh = new Mesh();
		mesh.MarkDynamic();

		vertexCount = 2 + (numPoints - 2) * 4 + 2;
		int triangleCount = (numPoints - 2) * 4 + 2;

		vertices = new Vector3[vertexCount];
		mesh.vertices = vertices;

		var uv = new Vector2[vertexCount];

		//first point
		uv[0] = new Vector2(0f,0f);
		uv[1] = new Vector2(1f,0f);

		for(int i = 1; i < numPoints - 1; i++)
		{
			float v = (i / (float)(numPoints - 1)) * length;
			var dv = thickness;
			uv[4*i-2+0] = new Vector2(0f,v-dv);
			uv[4*i-2+1] = new Vector2(1f,v-dv);
			uv[4*i-2+2] = new Vector2(0f,v+dv);
			uv[4*i-2+3] = new Vector2(1f,v+dv);
		}

		// last point
		uv[vertexCount-2] = new Vector2(0f,length);
		uv[vertexCount-1] = new Vector2(1f,length);

		mesh.uv = uv;
		var indices = new int[triangleCount * 3];
		for (int i = 0; i < triangleCount / 2;i++)
		{
			// 0, 1, 2
			// 1, 3, 2
			indices[6*i+0] = 2*i + 0;
			indices[6*i+1] = 2*i + 1;
			indices[6*i+2] = 2*i + 2;

			indices[6*i+3] = 2*i + 1;
			indices[6*i+4] = 2*i + 3;
			indices[6*i+5] = 2*i + 2;
		}
		mesh.triangles = indices;

		meshFilter = GetComponent<MeshFilter>();
		meshFilter.mesh = mesh;
	}

	// heavily inlined, using blocks to group things
	void LateUpdate () {
		var mesh = meshFilter.mesh;

		var numPoints = rope.numPoints;

		// set vertices for first point
		{
			var pos = rope.positions[0];
			var nextPos = rope.positions[1];
			var d = (nextPos - pos).normalized * thickness;
			var dOrtho = new Vector2(d.y, -1f * d.x);
			var v0 = pos + dOrtho;
			var v1 = pos - dOrtho;
			vertices[0] = new Vector3(v0.x, v0.y);
			vertices[1] = new Vector3(v1.x, v1.y);
		}

		// all of the inner points
		for(int i = 1; i < numPoints - 1; i++)
		{
			var prevPos = rope.positions[i-1];
			var pos = rope.positions[i];
			var nextPos = rope.positions[i+1];

			// vertices slightly "before" the point
			{
				var d = (pos - prevPos).normalized * thickness;
				var dOrtho = new Vector2(d.y, -1f * d.x);
				var v0 = pos - d + dOrtho;
				var v1 = pos - d - dOrtho;
				vertices[4*i-2+0] = new Vector3(v0.x, v0.y);
				vertices[4*i-2+1] = new Vector3(v1.x, v1.y);
			}

			// vertices slightly "after" the point
			{
				var d = (nextPos - pos).normalized * thickness;
				var dOrtho = new Vector2(d.y, -1f * d.x);
				var v0 = pos + d + dOrtho;
				var v1 = pos + d - dOrtho;
				vertices[4*i-2+2] = new Vector3(v0.x, v0.y);
				vertices[4*i-2+3] = new Vector3(v1.x, v1.y);
			}
		}

		// set vertices for last point
		{
			var pos = rope.positions[numPoints-2];
			var nextPos = rope.positions[numPoints-1];
			var d = (nextPos - pos).normalized * thickness;
			var dOrtho = new Vector2(d.y, -1f * d.x);
			var v0 = nextPos + dOrtho;
			var v1 = nextPos - dOrtho;
			vertices[vertexCount - 2] = new Vector3(v0.x, v0.y);
			vertices[vertexCount - 1] = new Vector3(v1.x, v1.y);
		}

		mesh.vertices = vertices;
		mesh.UploadMeshData(markNoLogerReadable:false);
		mesh.RecalculateBounds();
	}
}
